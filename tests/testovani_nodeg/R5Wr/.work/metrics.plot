set title "System logorg #0 disk Rotational latency average" font ",16"
#set label "-r1000 -S 1 -R 0.5 -s exponential,8.0,256.0 raid5 5" at screen 0.5, 0.88 center font "Arial,12"
unset key

set xlabel "threads"
set ylabel "value"
set xtic auto
set ytic auto
set grid xtics ytics
#set yrange [0:*]

plot "<cat" using 1:2 with lines
