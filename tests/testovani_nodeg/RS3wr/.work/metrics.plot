set title "System logorg Response time average" font ",16"
#set label "-r10000 -S 0 -R 0.0 -s exponential,8.0,8.0 raid5 5" at screen 0.5, 0.88 center font "Arial,12"
unset key

set xlabel "threads"
set ylabel "value"
set xtic auto
set ytic auto
set grid xtics ytics
set yrange [0:*]

plot "<cat" using 1:2 with lines
