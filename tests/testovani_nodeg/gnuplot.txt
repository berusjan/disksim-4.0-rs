#set title "System logorg Response time average" font ",16"
#set label "-r10000 -S 0 -R 1.0 -s exponential,8.0,8.0 raid5 5" at screen 0.5, 0.88 center font "Arial,12"
unset key

set xlabel "Počet vláken"
#set ylabel "Hodnota"
set ylabel "Propustnost [operace/s]"
#set ylabel "Response time [ms]"
set xtic auto
set ytic auto
set grid xtics ytics
set yrange [0:*]
set key autotitle columnhead


#plot "R0rd_prop.txt" using 1:2 with lines, "RNRrd_prop.txt" using 1:2 with lines,  "R0wr_prop.txt" using 1:2 with lines, "RNRwr_prop.txt" using 1:2 with lines

#plot "R0rd_resp.txt" using 1:2 with lines, "RNRrd_resp.txt" using 1:2 with lines,  "R0wr_resp.txt" using 1:2 with lines, "RNRwr_resp.txt" using 1:2 with lines

#plot "R5rd_resp.txt" using 1:2 with lines, "RPRrd_resp.txt" using 1:2 with lines,  "R5wr_resp.txt" using 1:2 with lines, "RPRwr_resp.txt" using 1:2 with lines

#plot "R5rd_prop.txt" using 1:2 with lines, "RPRrd_prop.txt" using 1:2 with lines,  "R5wr_prop.txt" using 1:2 with lines, "RPRwr_prop.txt" using 1:2 with lines




#plot "R6rd_prop.txt" using 1:2 with lines, "R6wr_prop.txt" using 1:2 with lines, "RS3rd_prop.txt" using 1:2 with lines, "RS3wr_prop.txt" using 1:2 with lines

plot "R0wr_prop.txt" using 1:2 with lines, "R5wr_prop.txt" using 1:2 with lines, "R6wr_prop.txt" using 1:2 with lines, "RS3wr_prop.txt" using 1:2 with lines

